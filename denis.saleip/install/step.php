<?

if (!check_bitrix_sessid()) {
    return;
}

if ($ex = $APPLICATION->getException()) {
    echo CAdminMessage::showMessage([
        'TYPE' => 'ERROR',
        'MESSAGE' => 'Ошибка установки модуля',
        'DETAILS' => $ex->getString(),
        'HTML' => true,
    ]);
} else {
    echo CAdminMessage::showNote('Модуль успешно установлен');
}

?>
<form action="<?
echo $APPLICATION->getCurPage(); ?>">
  <input type="submit" name="" value="Все модули">
</form>