<?php

use \Bitrix\Main\ {
    Loader,
    Application,
    ModuleManager,
    EventManager,
    Entity\Base
};
use Denis\SaleIP\SaleIPTable;

class denis_saleip extends CModule
{
    function __construct()
    {
        $arModuleVersion = array();
        include(__DIR__ . '/version.php');

        $this->MODULE_ID = 'denis.saleip';
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        $this->MODULE_NAME = 'IP покупателя';
        $this->MODULE_DESCRIPTION = 'Сохраняет IP покупателя при совершении заказа и отправляет на емэйл расширенные данные IP по базе RIPE.';

        $this->PARTNER_NAME = 'Denis Kirillov';
        $this->PARTNER_URI = 'https://spb.hh.ru/resume/9bd4677fff09be0da20039ed1f4237466f3669';
    }

    function installDB(): void
    {
        Loader::includeModule($this->MODULE_ID);

        if (
            !Application::getConnection()->isTableExists(
                Base::getInstance('\Denis\SaleIP\SaleIPTable')->getDBTableName()
            )
        ) {
            Base::getInstance('\Denis\SaleIP\SaleIPTable')->createDbTable();
        }
    }

    function unInstallDB(): void
    {
        Loader::includeModule($this->MODULE_ID);

        Application::getConnection()->
        queryExecute('drop table if exists ' . Base::getInstance('\Denis\SaleIP\SaleIPTable')->getDBTableName());
    }

    function installEvents(): void
    {
        EventManager::getInstance()->registerEventHandler(
            'sale',
            'OnOrderAdd',
            $this->MODULE_ID,
            '\Denis\SaleIP\Event',
            'orderHandler'
        );
    }

    function unInstallEvents(): void
    {
        EventManager::getInstance()->unRegisterEventHandler(
            'sale',
            'OnOrderAdd',
            $this->MODULE_ID,
            '\Denis\SaleIP\Event',
            'orderHandler'
        );
    }

    function installAgents(): void
    {
        CAgent::addAgent(
            '\Denis\SaleIP\Agent::saveRipeRecords();',
            'denis.saleip',
            'N',
            30
        );
    }

    function unInstallAgents(): void
    {
        CAgent::removeModuleAgents('denis.saleip');
    }

    function installMessages(): void
    {
        $et = new CEventType;
        $et->add([
            'LID' => SaleIPTable::getLid(),
            'EVENT_NAME' => 'DENIS_SALEIP_RIPE_RESPONSE',
            'NAME' => 'Данные IP покупателя',
            'DESCRIPTION' => 'Отправка данных IP покупателей последних заказов магазина администратору сайта.'
        ]);
    }

    function unInstallMessages(): void
    {
        $et = new CEventType;
        $et->delete('DENIS_SALEIP_RIPE_RESPONSE');
    }

    function doInstall(): void
    {
        global $APPLICATION;
        ModuleManager::registerModule($this->MODULE_ID);

        $this->installDB();
        $this->installEvents();
        $this->installAgents();
        $this->installMessages();

        $APPLICATION->includeAdminFile(
            'Установка модуля',
            dirname(__DIR__) . '/install/step.php'
        );
    }

    function doUninstall(): void
    {
        global $APPLICATION;

        $this->unInstallMessages();
        $this->unInstallAgents();
        $this->unInstallEvents();
        $this->unInstallDB();

        ModuleManager::unRegisterModule($this->MODULE_ID);

        $APPLICATION->includeAdminFile(
            'Удаление модуля',
            dirname(__DIR__) . '/install/unstep.php'
        );
    }
}

?>