<?

if (!check_bitrix_sessid()) {
    return;
}

if ($ex = $APPLICATION->getException()) {
    echo CAdminMessage::showMessage([
        'TYPE' => 'ERROR',
        'MESSAGE' => 'Ошибка удаления модуля',
        'DETAILS' => $ex->getString(),
        'HTML' => true,
    ]);
} else {
    echo CAdminMessage::showNote('Модуль успешно удалён');
}

?>
<form action="<?
echo $APPLICATION->getCurPage(); ?>">
  <input type="submit" name="" value="Все модули">
</form>