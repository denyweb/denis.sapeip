<?php

namespace Denis\SaleIP;

use Bitrix\Main\ {
    Entity,
    SiteTable,
    Mail,
    Config
};

class SaleIPTable extends Entity\DataManager
{
    public static function getTableName(): string
    {
        return 'denis_sale_ip';
    }

    public static function getMap(): array
    {
        return [
            new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\IntegerField('ORDER_ID', [
                'required' => true,
            ]),
            new Entity\StringField('IP', [
                'required' => true,
            ]),
            new Entity\TextField('RIPE'),
        ];
    }

    public static function onAfterUpdate(Entity\Event $event): void
    {
        $data = $event->getParameter('fields');

        if (isset($data['RIPE'])) {
            $subject = 'Заказ ' . $data['ORDER_ID'] . ', данные по IP: ' . $data['IP'];
            $message = RIPE::formatRecordForMail(unserialize($data['RIPE']));

            Mail\Event::send([
                'EVENT_NAME' => 'DENIS_SALEIP_RIPE_RESPONSE',
                'LID' => self::getLid(),
                'C_FIELDS' => [
                    'EMAIL_TO' => Config\Option::get('main', 'email_from'),
                    'SUBJECT' => $subject,
                    'MESSAGE' => $message
                ]
            ]);
        }
    }

    public static function getLid(): string
    {
        $site = SiteTable::getList([
            'select' => ['LID'],
            'filter' => ['ACTIVE' => 'Y']
        ])->fetch();
        return $site['LID'];
    }
}