<style type="text/css">
    .ripe {
        border-collapse: collapse;
    }

    .ripe th {
        text-align: left;
        border-bottom: 1px solid black;
        padding: 15px 5px 3px 5px;
    }

    .ripe td {
        padding: 3px 5px;
    }
</style>
<table class="ripe">
    <?
    foreach ($data as $section): ?>
      <tr>
        <th><?= $section['header']['type'] ?></th>
        <th><a href="<?= $section['header']['link'] ?>"><?= $section['header']['name'] ?></a></th>
      </tr>
        <?
        foreach ($section['lines'] as $line): ?>
          <tr>
            <td><?= $line['type'] ?></td>
            <td>
                <?
                if ($line['link']): ?>
                  <a href="<?= $line['link'] ?>"><?= $line['name'] ?></a>
                <?
                else: ?>
                    <?= $line['name'] ?>
                <?
                endif; ?>
            </td>
          </tr>
        <?
        endforeach; ?>
    <?
    endforeach; ?>
</table>