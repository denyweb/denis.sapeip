<?php

namespace Denis\SaleIP;

class Event
{
    public function orderHandler($orderId)
    {
        SaleIPTable::add(
            ['ORDER_ID' => $orderId, 'IP' => $_SERVER['REMOTE_ADDR']]
        );
    }
}