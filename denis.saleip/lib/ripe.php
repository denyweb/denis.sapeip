<?php

namespace Denis\SaleIP;

class RIPE
{
    public $ip;
    private $objects;

    public function __construct(string $ip)
    {
        $this->ip = $ip;
        $this->objects = self::getRecord($ip);
    }

    public static function getRecord(string $ip): array
    {
        $url = 'https://rest.db.ripe.net/search.json?query-string=' . $ip;
        $httpClient = new \Bitrix\Main\Web\HttpClient();
        $response = json_decode($httpClient->get($url));
        return $response->objects->object;
    }

    public static function formatRecordForMail(array $objects): string
    {
        $data = array();
        foreach ($objects as $i => $object) {
            $data[$i]['header'] = array(
                'type' => $object->type,
                'name' => $object->source->id,
                'link' => $object->link->href
            );

            foreach ($object->attributes->attribute as $j => $line) {
                $data[$i]['lines'][$j] = array(
                    'type' => $line->name,
                    'name' => $line->value
                );
                if (isset($line->link->href)) {
                    $data[$i]['lines'][$j]['link'] = $line->link->href;
                }
            }
        }

        ob_start();
        include(dirname(__FILE__) . '/email.tpl.php');
        $emailHtml = ob_get_clean();
        return $emailHtml;
    }
}