<?php

namespace Denis\SaleIP;

class Agent
{
    public static function saveRipeRecords(): string
    {
        $ripeRecords = self::getBlankRecords();
        if (is_array($ripeRecords) && count($ripeRecords) > 0) {
            foreach ($ripeRecords as $record) {
                $ripeData = RIPE::getRecord($record['IP']);
                $ripeData = serialize($ripeData);
                SaleIPTable::update(
                    $record['ID'],
                    ['RIPE' => $ripeData]
                );
            }
        }
        return '\Denis\SaleIP\Agent::saveRipeRecords();';
    }

    public static function getBlankRecords(): array
    {
        $result = SaleIPTable::getList(array(
            'select' => ['ID', 'IP'],
            'filter' => ['!==RIPE' => null]
        ))->fetchAll();
        return $result;
    }
}